<?php

return [
    'text' => 'Текст',
    'text.singular' => 'Текст',
    'text.plural'   => 'Тексты',

    'image' => 'Картинка',
    'image.singular' => 'Картинка',
    'image.plural' => 'Картинки',

    'slider' => 'Слайдер',
    'slider.singular' => 'Слайдер',
    'slider.plural' => 'Слайдеры',
];