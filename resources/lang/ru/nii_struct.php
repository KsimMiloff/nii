<?php

return [
    'institute' => 'Институт',
    'institute.leadership' => 'Руководство',
    'institute.aup' => 'АУП',
    'institute.technical-staff' => 'Технический персонал',

    'research' => 'Научный отдел',
    'research.integrated-system-protection' => 'Отдел внедрения интегрированной системы защиты растений',
    'research.integrated-system-protection.grains-legumes' => 'Группа защиты зерновых и зернобобовых культур',
    'research.integrated-system-protection.fruit-vegetables-melons' => 'Группа защиты плодовых и овощебахчевых культур',
    'research.integrated-system-protection.technical-oilseeds' => 'Группа защиты технических и масличных культур',
    'research.integrated-system-protection.forest-tree' => 'Группа защиты леса и древесных насаждений',

    'research.biological-protection' => 'Отдел биологических средств защиты растений',
    'research.biological-protection.beneficial-arthropods' => 'Лаборатория полезных членистоногих',
    'research.biological-protection.microbiology' => 'Лаборатория микробиолоии',

    'research.quarantine' => 'Отдел карантина растений',

    'research.phytosanitary-analysis' => 'Центр фитосанитарного лабораторного анализа',
    'research.phytosanitary-analysis.entomology' => 'Лаборатория энтомологии',
    'research.phytosanitary-analysis.phytopathology' => 'Лаборатория фитопатологии',
    'research.phytosanitary-analysis.toxicology-of-pesticides' => 'Лаборатория токсикологи и пестицидов',
    'research.phytosanitary-analysis.genetics-biochemistry' => 'Лаборатория молекулярной генетики и биохимии',
    'research.phytosanitary-analysis.herbology' => 'Лаборатория гербологии',

    'research.kostanay' => 'Костанайский филиал',

    'research.south-kazakhstan' => 'Южно-Казахстанский филиал',
    'research.south-kazakhstan.quality-biological-agents' => 'Лаборатория определения качества биоагентов',
    'research.south-kazakhstan.ontustik' => 'Биофабрика «Оңтүстік»',
];