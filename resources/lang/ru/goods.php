<?php

return [
    'model' => 'Товар',

    'product' => 'Товар',
    'products.singular' => 'Продукция',
    'products.plural'   => 'Продукция',


    'service' => 'Услуга',
    'services.singular' => 'Услуга',
    'services.plural'   => 'Услуги',

    'site_menu_title' => 'Продукция и услуги',
];