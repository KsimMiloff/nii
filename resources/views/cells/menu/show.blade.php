<nav id="menu">
    @foreach($menu_items as $item)
        <a class="{!! $item['active'] ? 'active' : '' !!}" href="{!! $item['url'] !!}">{!! $item['title'] !!}
            <span class="hover"></span>
        </a>
    @endforeach
</nav>