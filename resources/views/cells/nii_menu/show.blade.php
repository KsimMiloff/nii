<nav class="nii-struct {!! $class !!}">
    @foreach( $top_node->branches as $subnode)

        <?php $class = ''; ?>

        @if ( $subnode->is_ancestor_of( $current_node ) || $subnode == $current_node )
            <?php $class = 'active'; ?>

            @if (count( $subnode->branches ))
                <?php $sub_nav = NiiMenuCell::nav_subs($subnode, $current_node); ?>
            @endif
        @endif

        <a href="{!! locale_route('nii.index', ['chain' => $subnode->path()]) !!}" class="{!! $class !!}">
            {!! $subnode->title() !!}
        </a>
    @endforeach
</nav>


@if (isset( $sub_nav ))
    {!! $sub_nav !!}
@endif
