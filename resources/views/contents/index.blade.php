@extends('layouts.site')


@include('partials._seotags', ['seotags' => (object)[
    'title' => trans("seotags.news.title"),
    'keywords' => trans("seotags.news.title"),
    'description' => trans("seotags.news.title")
]])

@section('content')

    <section class="content p-47">
        <h1 class="h h1">{!! $category->title('plural') !!}

            @unless($years['end'] == $years['start'])
                <small class="years-menu">
                    <br>
                    @for($year = $years['end']; $year >= $years['start']; $year-- )
                        <a class="{!! $year == $current_year ? 'active' : '' !!}"
                           href="{!! locale_route('contents.index', ['category_id' => $category->id, 'year' => $year]) !!}">
                            {!! $year !!}
                        </a>
                    @endfor
                    </small>
            @endunless

        </h1>
    </section>

    @foreach( $contents as $content )
        <section class="p-47 news-list">
            <div class="wysiwyg">

                <div class="news-list-item">
                    <div class="news-image">
                        <a href="{!! locale_route('contents.show', ['category_id' => $category->id, 'id' => $content->seo_id]) !!}">
                            {!! Html::picture($content->image_id, ['size' => '251x193', 'crop' => true]) !!}
                        </a>
                        <span class="date">
                            {!! Carbon\Carbon::parse($content->created_at)->format('d.m.Y') !!}
                        </span>
                    </div>

                    <div class="news-text">
                        <div class="fadded">
                            <h2 class="h h2">
                                <a href="{!! locale_route('contents.show', ['category_id' => $category->id, 'id' => $content->seo_id]) !!}">{!! $content->title !!}</a>
                            </h2>
                            <div>
                                {!! str_limit( $content->desc, 300 ) !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    @endforeach


@stop
