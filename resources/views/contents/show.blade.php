@extends('layouts.site')

@include('partials._seotags', ['seotags' => $content->seotags()])

@section('content')

    <section class="content p-47 pt-30 wysiwyg">
        <h1 class="h h1">{!! $content->title !!}</h1>


        {!! Html::picture($content->image_id, ['size' => '400x300', 'crop' => true]) !!}
        <div>
            {!! $content->desc !!}
        </div>


    </section>

@stop
