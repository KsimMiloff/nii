<div class="btn-group">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">{{ !empty( $current_node->chain) ? $current_node->title() : 'Рубрики' }}
        <span class="caret"></span></button>

    @include('admin.nii.structure._tree', ['node' => $node])


</div>