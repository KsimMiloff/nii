<?php
    $current_node = array_shift( $current_nodes );
    if (count( $current_nodes ))
    {
        $title = $current_nodes[0]->title();
    }
    else {
        $title = '';
    }

?>

@if (count( $current_node->branches ))

    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
            {!! $title !!}

            @if (count( $current_node->branches ))
                <span class="caret"></span>
            @endif

        </button>


            <ul class="dropdown-menu">
                @foreach( $current_node->branches as $node )

                    <li>
                        <a href={!! locale_route('admin.nii.index', ['locale' => $current_locale, 'chain' => $node->path()]) !!}>
                            {!! $node->title() !!}
                        </a>
                    </li>

                @endforeach

            </ul>

    </div>
@endif

@if (count( $current_nodes ))
    @include('admin.nii.structure._dropdowns', ['current_nodes' => $current_nodes])
@endif
