@if ( count( $node->branches ) )
    <ul class="dropdown-menu">
        @foreach($node->branches as $subnode)

            <li class="{!! count( $subnode->branches ) ? 'dropdown-submenu' : '' !!}">
                <a href="{!! locale_route('admin.nii.index', ['locale' => $current_locale, 'chain' => $subnode->path()]) !!}">
                    {!! $subnode->title() !!}
                </a>

                @if (count( $subnode->branches ))
                    @include('admin.nii.structure._tree', ['node' => $subnode])
                @endif

            </li>
        @endforeach

    </ul>
@endif