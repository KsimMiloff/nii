@extends('layouts.admin')

@section('content')

    <div class="row">

        <h1>
            Структура НИИ
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                    {!! $current_locale !!} <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    @foreach( array_except( App\Lib\Locale::for_select(), [$current_locale] ) as $locale )
                        <li>
                            <a href={!! locale_route('admin.nii.index', ['locale' => $locale, 'chain' => $current_node->path()]) !!}>
                                {!! $locale !!}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </h1>

        @include('admin.nii.structure._dropdowns', ['current_nodes' => $current_nodes])

        @if( $current_node )
            <br/>
            <h2>
                {!! $current_node->title() !!}

                <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Добавить секцию <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @foreach ( $categories as $category )
                            <li>
                                <a href="#"
                                   data-behavior="add_section_content"
                                   data-url="{!! locale_route('admin.sections.create', ['locale' => $current_locale, 'category_id' => $category->id, 'chain' => $current_node->path()]) !!}">
                                    {!! $category->title('singular') !!}
                                </a>
                                {{--<button class="btn btn-link" >Добавить секцию</button>--}}
                            </li>
                        @endforeach
                    </ul>
                </div>

                {{--<button class="btn btn-primary" data-behavior="add_section_content" data-url="{!! locale_route('admin.sections.create', ['chain' => $current_node->path()]) !!}">Добавить секцию</button>--}}
            </h2>


            @include('admin.sections._list', ['sections' => $sections])

        @endif

    </div>

    <!-- Modal -->
    <div class="modal fade" id="content-form-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>This is a large modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@stop