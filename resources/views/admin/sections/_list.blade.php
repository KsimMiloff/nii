
<style>
    .handler {
        cursor: pointer;
    }
</style>

<div id="section_list"  class="list-group" data-orderize-url="{!! route('admin.sections.orderize') !!}">
    @foreach($sections as $section)
        @include('admin.sections._list_item', ['section' => $section])
    @endforeach
</div>
