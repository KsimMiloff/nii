{!! Form::resource($section, ['route' => 'admin.sections', 'class' => 'form-horizontal']) !!}

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">{!! $section->exist ? 'Редактирование' : 'Новая секция' !!}</h4>
</div>

<div class="modal-body">

    {!! Form::hidden('section[locale_id]', $section->locale_id) !!}
    {!! Form::hidden('section[category_id]', $section->category_id) !!}
    {!! Form::hidden('section[chain]', $section->chain) !!}


    <div class="tab-section">

        @include('admin.partials.errors', ['errors' => $section->errors()])

        @include("admin.sections.form._info_tab")

    </div>

</div>

<div class="modal-footer">
    {!! Form::submit('Сохранить', ['name' => 'action[save]', 'class' => 'btn btn-primary', 'data-behavior' => 'save_section']) !!}
    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
</div>


{!! Form::close() !!}
