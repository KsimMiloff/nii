<i>{!! str_limit( $section->small_desc, 100 ) !!}</i>
<br>

@if ( count( $section->slider_ids ) )
    @foreach( $section->slider_ids as $image_id)
        <div class="thumbnail" style="width: 60px; float: left; margin: 2px 4px 0 0">
            {!! Html::thumbed_link_to_picture($image_id, ['target' => 'blank'], ['size' => '50', 'crop' => true]) !!}
        </div>
    @endforeach
@endif
