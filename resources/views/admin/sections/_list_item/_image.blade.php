<i>{!! str_limit( $section->small_desc, 100 ) !!}</i>
<br>

<div class="thumbnail" style="width: 60px;">
    @if ($section->image_id)
        {!! Html::thumbed_link_to_picture($section->image_id, ['target' => 'blank'], ['size' => '50', 'crop' => true]) !!}
    @else
        <div class="thumb-empty"><span class="glyphicon glyphicon-minus"></span></div>
    @endif
</div>