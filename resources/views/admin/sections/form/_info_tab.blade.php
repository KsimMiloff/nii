<div id="info" class="tab-pane fade in active">

    <div class="row top-buffer">

        <div class="form-group">
            {!! Form::label('section[title]', 'Заголовок', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::text('section[title]', $section->title, array('class' => 'form-control')) !!}
            </div>
        </div>


        @foreach($section->category->fields() as $alias => $field)
            {!! $field->widget(['model' => 'section', 'alias' => $alias, 'value' => $section->$alias]) !!}
        @endforeach

    </div>
</div>