<div class="list-group-item" data-id="{!! $section->id !!}">

    <table width="100%">
        <colgroup>
            <col width="2%">
            <col width="2%">
            <col width="85%">
            <col width="5%">
        </colgroup>
        <tr>
            <td valign="top"><small class="glyphicon glyphicon-sort handler"></small></td>
            <td valign="top">
                <small>
                    <a href="#" class="glyphicon glyphicon-pencil"
                       data-behavior="edit_section_content"
                       data-url="{!! locale_route('admin.sections.edit', ['id' => $section->id]) !!}">
                    </a>
                </small>
            </td>
            <td valign="top">
                <b>{!! $section->title !!}</b>
                @include("admin.sections._list_item._{$section->category_id}", ['section' => $section])

            </td>
            <td valign="top">
                <a href="#" class="pull-right glyphicon glyphicon-trash"
                   data-behavior="delete_section_content"
                   data-url="{!! locale_route('admin.sections.destroy', ['id' => $section->id]) !!}"
                   data-id="{!! $section->id !!}">
                </a>
            </td>
        </tr>

    </table>

</div>
