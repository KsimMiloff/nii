@extends('layouts.admin')

@section('content')

    <h1>
        {!! $page->category->title() !!}
    </h1>


    <div class="row">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#info">Инфо</a></li>
            <li class=""><a data-toggle="tab" href="#seo">SEO</a></li>
        </ul>


        {!! Form::resource($page, ['route' => 'admin.single_pages', 'class' => 'form-horizontal']) !!}

        {!! Form::hidden('single_page[category_id]', $page->category_id) !!}

        <div class="tab-content">

            @include('admin.partials.errors', ['errors' => $page->errors])

            @include("admin.single_pages.form._info_tab")
            @include("admin.partials.form.seoable_tab._localed", ['seoable' => $page, 'seoble_type' => 'single_page'])

        </div>

        @include("admin.partials.form._button_bar", ['object' => $page, 'lifecycled' => true])


        {!! Form::close() !!}

    </div>

@stop

