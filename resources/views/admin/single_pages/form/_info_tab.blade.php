<div id="info" class="tab-pane fade in active">

    <div class="row top-buffer">

        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                <ul class="nav nav-pills">
                    @foreach(App\Lib\Locale::$list as $lang => $title)
                        <li class="{!! $lang == 'ru' ? 'active' : '' !!}"><a data-toggle="tab" href="#{!! $lang !!}">{!! $lang !!}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="tab-content">

            @foreach(['ru', 'en', 'kz'] as $lang)

                <div id="{!! $lang !!}" class="tab-pane fade in {!! $lang == 'ru' ? 'active' : '' !!}">

                    <div class="row top-buffer">

                        <div class="form-group">
                            {!! Form::label('title', "Заголовок ({$lang})", ['class' => 'control-label col-sm-2'])  !!}
                            <div class="col-sm-8">
                                {!! Form::text("single_page[title_{$lang}]", $page->title($lang), array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('desc', "Описание ({$lang})", ['class' => 'control-label col-sm-2'])  !!}
                            <div class="col-sm-8">
                                {!! Form::wisiwyg("single_page[desc_{$lang}]", $page->desc($lang), array('class' => 'form-control')) !!}
                            </div>
                        </div>

                    </div>
             </div>

            @endforeach

        </div>


        @foreach($page->category->fields() as $alias => $field)
            @if (! starts_with($alias, 'seo_'))
                {!! $field->widget(['model' => 'single_page', 'alias' => $alias, 'value' => $page->$alias]) !!}
            @endif
        @endforeach

        {{--<div class="form-group">--}}
            {{--<div class="col-sm-3 col-sm-offset-2">--}}
                {{--<div class="checkbox">--}}
                    {{--<label>--}}
                        {{--{!! Form::boolean('single_page[is_visible]', $page->is_visible) !!} Показывать на сайте--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}


    </div>
</div>