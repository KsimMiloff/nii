<div class="btn-group">

    <?php
    $state_menu = [
            'live' => 'Живые',
            'archive' => 'Архивные'
    ];

    $state = request()->get('state', 'live');
    $active_state = $state ? $state : 'live';
    ?>

    <select name="state" class="form-control">
        @foreach($state_menu as $state => $state_title)
            <option value="{!! $state !!}" {!! $active_state == $state ? 'selected' : '' !!}>
                {!! link_to_route($route, $state_title, ['state' => $state ], []) !!}
            </option>
        @endforeach
    </select>

</div>