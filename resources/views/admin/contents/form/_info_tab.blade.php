<div id="info" class="tab-pane fade in active">

    <div class="row top-buffer">

        <div class="form-group">
            {!! Form::label('content[locale_id]', 'Локаль', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-2">

                @if ($content->locale_id)
                    <div class="form-control-static">
                        {!! $content->locale_id !!}
                    </div>

                    {!! Form::hidden('content[locale_id]', $content->locale_id) !!}
                @else
                    {!! Form::select('content[locale_id]', App\Lib\Locale::for_select(), $content->locale_id, ['class' => 'form-control']) !!}
                @endif
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('content[title]', 'Заголовок', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::text('content[title]', $content->title, array('class' => 'form-control')) !!}
            </div>
        </div>


        @foreach($content->category->fields() as $alias => $field)
            @if (! starts_with($alias, 'seo_'))
                {!! $field->widget(['model' => 'content', 'alias' => $alias, 'value' => $content->$alias]) !!}
            @endif
        @endforeach


        <div class="form-group">
            {!! Form::label('content[desc]', 'Описание', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::wisiwyg('content[desc]', $content->desc, array('class' => 'form-control')) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-2">
                <div class="checkbox">
                    <label>
                        {!! Form::boolean('content[is_visible]', $content->is_visible) !!} Показывать на сайте
                    </label>
                </div>
            </div>
        </div>

    </div>
</div>