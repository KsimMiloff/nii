<div id="info" class="tab-pane fade in active">

    <div class="row top-buffer">

        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                <ul class="nav nav-pills">
                    @foreach(App\Lib\Locale::$list as $lang => $title)
                        <li class="{!! $lang == 'ru' ? 'active' : '' !!}"><a data-toggle="tab" href="#{!! $lang !!}">{!! $lang !!}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="tab-content">

            @foreach(['ru', 'en', 'kz'] as $lang)

                <div id="{!! $lang !!}" class="tab-pane fade in {!! $lang == 'ru' ? 'active' : '' !!}">

                    <div class="row top-buffer">

                        <div class="form-group">
                            {!! Form::label('name', "Название ({$lang})", ['class' => 'control-label col-sm-2'])  !!}
                            <div class="col-sm-8">
                                {!! Form::text("good[name_{$lang}]", $good->name($lang), array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('extra_text', "Пояснение ({$lang})", ['class' => 'control-label col-sm-2'])  !!}
                            <div class="col-sm-8">
                                {!! Form::text("good[extra_text_{$lang}]", $good->extra_text($lang), array('class' => 'form-control', 'rows' => 5)) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            {!! Form::label('desc', "Описание ({$lang})", ['class' => 'control-label col-sm-2'])  !!}
                            <div class="col-sm-8">
                                {!! Form::wisiwyg("good[desc_{$lang}]", $good->desc($lang), array('class' => 'form-control')) !!}
                            </div>
                        </div>

                    </div>
             </div>

            @endforeach

        </div>



        @foreach($good->category->fields() as $alias => $field)
            @if (! starts_with($alias, 'seo_'))
                {!! $field->widget(['model' => 'good', 'alias' => $alias, 'value' => $good->$alias]) !!}
            @endif
        @endforeach


        <div class="form-group">
            {!! Form::label('image_id', 'Изображение', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::images('good[image_id]', $good->image_id) !!}
            </div>
        </div>

        {{--<div class="form-group">--}}
        {{--{!! Form::label('slider_ids', 'Слайдер', ['class' => 'control-label col-sm-2'])  !!}--}}
        {{--<div class="col-sm-8">--}}
        {{--{!! Form::images('good[slider_ids]', $good->slider_ids, ['multiple' => true]) !!}--}}
        {{--</div>--}}
        {{--</div>--}}

        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-2">
                <div class="checkbox">
                    <label>
                        {!! Form::boolean('good[is_visible]', $good->is_visible) !!} Показывать на сайте
                    </label>
                </div>
            </div>
        </div>


    </div>
</div>