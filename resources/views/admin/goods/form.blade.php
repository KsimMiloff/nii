@extends('layouts.admin')

@section('content')

    <h1>
        {!! AdminGoodCrumbs::items( ['good' => $good ] )->last()['title']  !!}
    </h1>

    {!! AdminGoodCrumbs::show( ['good' => $good ] ) !!}

    <div class="row">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#info">Инфо</a></li>
            <li class=""><a data-toggle="tab" href="#seo">SEO</a></li>
            {{--<li class=""><a data-toggle="tab" href="#seted">Сопутствующие товары</a></li>--}}
        </ul>


        {!! Form::resource($good, ['route' => 'admin.goods', 'class' => 'form-horizontal']) !!}

        {!! Form::hidden('good[category_id]', $good->category_id) !!}

        <div class="tab-content">

            @include('admin.partials.errors', ['errors' => $good->errors])

            @include("admin.goods.form._info_tab")
            @include("admin.partials.form.seoable_tab._localed", ['seoable' => $good, 'seoble_type' => 'good'])
            {{--@include("admin.goods.form._seted_tab")--}}

        </div>

        @include("admin.partials.form._button_bar", ['object' => $good, 'lifecycled' => true])


        {!! Form::close() !!}

    </div>

@stop

