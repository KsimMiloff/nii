@extends('layouts.admin')

@section('content_bar')

    {!! AdminFilterCell::show([
        'category' => ['categories' => $categories],
        'lifecycle' => ['route' => 'admin.contents.index']
        ])
    !!}

@stop

@section('content')

    <div class="row">

        {{--<div class="col-md-2">--}}
        {{--</div>--}}

        <h1>Товары

            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Добавить... <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    @foreach ( $categories as $category )
                        <li> {!! link_to_route('admin.goods.create', $category->title(), ['category_id' => $category->id], []) !!} </li>
                    @endforeach
                </ul>
            </div>

        </h1>

        @if ($goods->isEmpty())
            <p>Нет ни одного товара</p>

        @else

            <table class="table table-hover">
                <colgroup>
                    <col width="70px"/>
                </colgroup>
                <thead>
                    <tr>
                        <th></th>
                        <th>Название</th>
                        <th>Категория</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($goods as $good)
                        <tr>
                            {!! Form::resource($good, ['route' => 'admin.goods', 'route_params' => ['redirect_to' => 'index'], 'class' => 'form-horizontal']) !!}

                            <td>
                                <div class="thumbnail">
                                    @if ($good->image_id)
                                        {!! Html::thumbed_link_to_picture($good->image_id, ['target' => 'blank'], ['size' => '50', 'crop' => true]) !!}
                                    @else
                                        <div class="thumb-empty"><span class="glyphicon glyphicon-minus"></span></div>
                                    @endif
                                </div>
                            </td>
                            <td> {!! link_to_route('admin.goods.edit', $good->name, ['good_id' => $good->id]) !!} </td>
                            <td> {!! $good->category->title() !!} </td>

                            <td>
                                <div class="pull-right">
                                    @if ($good->is_live)
                                        {!! Form::submit('В архив', ['name' => 'action[move_to_archive]', 'class' => 'btn btn-primary']) !!}
                                    @elseif ($good->is_archive)
                                        {!! Form::submit('Восстановить', ['name' => 'action[move_to_live]', 'class' => 'btn btn-primary']) !!}
                                    @endif
                                </div>

                            </td>

                            {!! Form::close() !!}
                        </tr>

                    @endforeach

                </tbody>
            </table>

        @endif

    </div>
@stop