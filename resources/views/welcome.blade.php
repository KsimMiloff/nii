@extends('layouts.site')

@include('partials._seotags', ['seotags' => $page->seotags()])

@section('left-col')
    <nav id="nav" class="nav default-nav">
        <a class="ico-news" href="{!! locale_route('contents.index', ['category_id' => 'news']) !!}">
            {!! trans('contents.news.plural') !!}
        </a>
        <a class="ico-goods" href="{!! locale_route('goods.index', ['category_id' => 'products']) !!}">
            {!! trans('goods.site_menu_title') !!}
        </a>
        <a class="ico-contacts" href="{!! locale_route('single_pages.show', ['category_id' => 'contacts']) !!}">
            {!! trans('single_pages.contacts.singular') !!}
        </a>
    </nav>
@stop


@section('content')

    <section class="slider">

        @if ( count( $page->slider_ids ) )
            <div class="fotorama"  data-width="1095px" data-height="345" data-autoplay="true">
                @foreach( $page->slider_ids as $image_id)
                    {!! Html::picture($image_id, ['size' => '1095x345', 'crop' => true]) !!}
                @endforeach
            </div>
        @endif

    </section>

    <section class="content p-47 wysiwyg">

        <h1 class="h h1">
            {{ $page->title  }}
        </h1>

        {!! $page->desc !!}
    </section>

    @if (count( $news ))

        <section class="content news">
            <div class="p-47">
                <h2 class="h h2 ico ico-news">{!! trans('contents.news.plural') !!}</h2>
            </div>

            <div class="grid">

                @foreach($news as $n)
                    <div class="grid-cell news-item">
                        <div class="lr-borders">
                            <div class="grig-cell-content">
                                <a href="{!! locale_route('contents.show', ['category_id' => $n->category_id, 'id' => $n->seo_id]) !!}">

                                    @if (!empty ( $n->title ) )
                                        {!! str_limit($n->title, 35) !!}
                                        <br/>
                                    @endif

                                    {!! Html::picture($n->image_id, ['size' => '250x190']) !!}

                                </a>

                                @if (!empty ( $n->created_at ) )
                                    <br/>
                                    <span class="date">{!! Carbon\Carbon::parse($n->created_at)->format('d.m.Y') !!}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>

        </section>
    @endif

    <section class="pt-30">
        <div class="col">
            <div class="nii-list">
                <div class="nii-list-wrapper">
                    <h3 class="h h2">
                        {!! $node->title() !!}
                    </h3>

                    <nav class="nii-menu">

                        @foreach($node->branches as $subnode)
                            <a href="{!! locale_route('nii.index', ['chain' => $subnode->path()]) !!}">{!! $subnode->title() !!}</a>
                        @endforeach

                    </nav>
                </div>
            </div>

        </div>

        <div class="col product-slider">
            <h3 class="h h1 ico ico-goods">
                {!! trans('goods.products.plural') !!}
            </h3>
            <div>
                {{--<div class="fotorama" data-maxwidth="320" data-autoplay="true">--}}
                {{--<img src="http://s.fotorama.io/1.jpg">--}}
                {{--<img src="http://s.fotorama.io/2.jpg">--}}
                {{--</div>--}}
            </div>
        </div>
    </section>
@stop
