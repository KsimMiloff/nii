@extends('layouts.site')

<?php $t_chain = implode($node->chain) ?>

@include('partials._seotags', ['seotags' => (object)[
    'title' => Lang::has('message.welcome') ? trans("seotags.$t_chain.title") : $node->title(),
    'keywords' => trans("seotags.$t_chain.title"),
    'description' => trans("seotags.$t_chain.title")
]])

@section('left-col')
    @if (count ( $sections ) > 1)
        <nav class="nav sections-nav">
            <a href="#nii-{{ $node->id  }}">{{ $node->title()  }}</a>

            @foreach($sections as $section)
                @unless(empty( $section->title ))
                    <a href="#section-{{ $section->id  }}">{{ trim((string) $section->nav_title) ? $section->nav_title : $section->title  }}</a>
                @endunless
            @endforeach
        </nav>
    @endif

@stop

@section('content')
    {!! NiiMenuCell::show( $node ) !!}

    <section class="content p-47 pt-30 wysiwyg">
        <h1 class="h h1" id="nii-{{ $node->id  }}">{!! $node->title() !!}</h1>

        @include('nii._sections', ['sections' => $sections])
    </section>

@stop
