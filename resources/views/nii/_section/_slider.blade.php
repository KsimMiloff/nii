
@if ( count( $section->slider_ids ) )
    <div class="fotorama"  data-width="1000px" data-height="345" data-autoplay="true">
        @foreach( $section->slider_ids as $image_id)
            {!! Html::picture($image_id, ['size' => '1000x345', 'crop' => true]) !!}
        @endforeach
    </div>
@endif
