<div>
    @if (!empty ( $section->title ))
        <h2 class="h h2" id="section-{!! $section->id !!}"> {!! $section->title !!}</h2>
    @endif

    @if (!empty ( $section->small_desc))
        <i class="italic">{!! $section->small_desc !!}</i>
    @endif

    @include("nii._section._{$section->category_id}", ['section' => $section])
</div>