@extends('layouts.site')

@include('partials._seotags', ['seotags' => $page->seotags()])


@section('content')

    <section class="content p-47 wysiwyg">

        <h1 class="h h1">
            {{ $page->title  }}
        </h1>

        {!! $page->desc !!}

        {!! $page->map_code !!}
    </section>

@stop
