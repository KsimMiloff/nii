<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        @yield('seotags')

        <link rel="icon" type="image/png" href="{{ URL::asset('assets/images/favicon.ico') }}" />

        <link href="{{ asset('/vendor/styles/reset.css') }}" rel="stylesheet">
        <link href="{{ asset('/vendor/styles/fotorama.css') }}" rel="stylesheet">
        <link href="{{ asset('/vendor/styles/layout.css') }}" rel="stylesheet">
        <link href="{{ asset('/assets/styles/site/main.css') }}" rel="stylesheet">
        <link href="{{ asset('/assets/styles/site/wysiwyg.css') }}" rel="stylesheet">

        <script src="{{ asset('/vendor/scripts/jquery.js') }}"></script>
        <script src="{{ asset('/vendor/scripts/fotorama.js') }}"></script>
        <script src="{{ asset('/assets/scripts/site/site.js') }}"></script>

        @yield('styles')

        @yield('scripts')
    </head>
    <body>

        <main class="page-row page-row-expanded">
            <div class="main-table">

                <div class="col" id="nav-col">
                    <div class="logo">
                        <a href="/">
                            <img src="{{ URL::asset('assets/images/logo.png') }}">
                        </a>
                    </div>

                    @yield('left-col')

                </div>

                <section class="col page">

                    {!! MenuCell::show() !!}

                    <nav class="lang-nav">
                        <a href="/ru" class="{{ App::getLocale() == 'ru' ? 'active' : '' }}">рус</a>
                        <a href="/en" class="{{ App::getLocale() == 'en' ? 'active' : '' }}">eng</a>
                        <a href="/kz" class="{{ App::getLocale() == 'kz' ? 'active' : '' }}">каз</a>
                    </nav>


                    @yield('content')

                </section>
            </div>
        </main>

        <footer class="page-row">
            <div class="main-table">
                <div class="col spacer"></div>
                <div class="col copyright">
                    <p>
                        &copy; {!! 2016 == date("Y") ? date("Y") : "2016-" . date("Y") !!} Казахстанский Научно-Исследовательский Институт
                        Защиты и Карантина Растений
                    </p>
                </div>
            </div>
        </footer>

    </body>
</html>