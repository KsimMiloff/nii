@extends('layouts.site')

@include('partials._seotags', ['seotags' => (object)[
    'title' => trans("seotags.goods.title"),
    'keywords' => trans("seotags.goods.title"),
    'description' => trans("seotags.goods.title")
]])

@section('content')

    <section class="content news">
        <div class="p-47">
            <div class="goods-nav">
                <a href="{!! locale_route('goods.index', ['category_id' => 'products']) !!}" class="{!! $category_id == 'products' ? 'active' : '' !!}">Продукция</a>
                <span>и</span>
                <a href="{!! locale_route('goods.index', ['category_id' => 'services']) !!}" class="{!! $category_id == 'services' ? 'active' : '' !!}">услуги</a>
            </div>
        </div>

        <div class="grid">

            @foreach($goods as $good)
                <div class="grid-cell good-item">
                    <div class="lr-borders">
                        <div class="grig-cell-content">
                            <a href="#">
                                {!! Html::picture($good->image_id, ['size' => '205x205']) !!}

                                @if (!empty ( $good->name ) )
                                    <br/>
                                    {!! $good->name !!}
                                @endif
                            </a>
                            @if (!empty ( (float) $good->price ) )
                                <br/>
                                <span class="price">{!! (int) $good->price !!} тг.</span>
                            @endif
                        </div>
                    </div>
                </div>

            @endforeach
        </div>

    </section>

@stop
