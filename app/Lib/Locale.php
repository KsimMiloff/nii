<?php

namespace App\Lib;

class Locale
{
    public static $list = [
        'ru' => 'русский',
        'en' => 'английский',
        'kz' => 'казахский'
    ];

    public static function for_select() {
        $arr = [];
        foreach( array_keys( static::$list ) as $id ) {
            $arr[$id] = $id;
        }
        return $arr;
    }
}
