<?php

namespace App\Lib\XFields\Field;

use Form;

class SquareField extends BaseField
{

    public function from_db_format($value)
    {
        return is_array($value) ? $value : [null, null];
    }

    public function to_db_format( $value )
    {
        if ( $this->can_be_valid( $value ) ) {

            ! is_numeric($value['w']) ?: array_except($value, ['w']);
            ! is_numeric($value['h']) ?: array_except($value, ['h']);

            return $value;
        }

        return null;
    }

    public function can_be_valid($value) {
        if ( is_array( $value ) ) {
            $value = collect( $value );

            if ( $value->has( 'w' ) || $value->has( 'h' ) ) {
                return true;
            }
        }

        return false;
    }

    public function field($field_alias, $value) {
        $w = array_get($value, 'w', null);
        $h = array_get($value, 'h', null);
        $s = (float)($w) * (float)($h);

        $w = Form::number($field_alias . '[w]', $w, ['class' => 'form-control', 'step' => "0.01"]);
        $h = Form::number($field_alias . '[h]', $h, ['class' => 'form-control', 'step' => "0.01"]);
        $sep = "<div style='text-align: center;'><label class='control-label'> x </label></div>";

        $elements = [ $w => 3, $sep => 2, $h => 3];

        if ($s > 0) {
            $s     = "<div class='form-control' style='background-color: #efefef;'>$s</div>";
            $equal = "<div style='text-align: center;'><label class='control-label'> = </label></div>";

            $elements[$equal] = 2;
            $elements[$s] = 3;
        };

        $html = '';
        foreach ( $elements as $el => $width ) {
            $html .= "<div class='row col-xs-{$width}''>{$el}</div>";
        }

        return $html;
    }

    public function widget($options, $class=null)
    {
        $class = is_null($class) ? 'col-sm-5' : $class;

        $value = array_get( $options, 'value', null );
        $value = is_array($value) ? $value : [];

        $options['value'] = $value;

        return parent::widget($options, $class);
    }

}
