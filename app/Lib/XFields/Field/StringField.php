<?php

namespace App\Lib\XFields\Field;

use Form;

class StringField extends TextField
{

    public function field($field_alias, $value=null) {
        return Form::text($field_alias, $value, ['class' => 'form-control']);
    }

}
