<?php

namespace App\Lib\XFields\Category;

use App\Lib\XFields\Field\BaseField;
use App\Lib\XFields\Category\SeoableCategory;

class ContentCategory extends SeoableCategory
{

    public static function hash() {

        return [
            'news' => 'contents.news',
        ];
    }


    public static function category_fields()
    {
        return [
            'news'  => [

                'image_id' => BaseField::create([
                    'title'   => 'Картинка',
                    'storage' => 'image_id',
                    'type'    => 'Image',
                ]),

            ],

        ];
    }

}
