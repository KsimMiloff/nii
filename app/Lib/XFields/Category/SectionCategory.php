<?php

namespace App\Lib\XFields\Category;

use App\Lib\XFields\Field\BaseField;
use App\Lib\XFields\Category\Base;

class SectionCategory extends Base
{

    public static function hash() {
        return [
            'text' => 'sections.text',
            'image' => 'sections.image',
            'slider' => 'sections.slider'
        ];
    }


    public static function config()
    {
        return [
            'text'  => [

                'desc' => BaseField::create([
                    'title'   => 'Текст',
                    'storage' => 'props.desc',
                    'type'    => 'Wisiwyg',
                ]),

                'nav_title' => BaseField::create([
                    'title'   => 'Заголовок для навигации',
                    'storage' => 'props.nav_title',
                    'type'    => 'String',
                ]),
            ],


            'image' => [
                'image_id' => BaseField::create([
                    'title'   => 'Картинка',
                    'storage' => 'props.image_id',
                    'type'    => 'Image',
                    'is_json' => true,
                ]),

                'small_desc' => BaseField::create([
                    'title'   => 'Текст над картинкой',
                    'storage' => 'props.small_desc',
                    'type'    => 'Text',
                ]),

                'nav_title' => BaseField::create([
                    'title'   => 'Заголовок для навигации',
                    'storage' => 'props.nav_title',
                    'type'    => 'String',
                ]),
            ],

            'slider' => [
                'slider_ids' => BaseField::create([
                    'title'   => 'Слайдер',
                    'storage' => 'props.image_ids',
                    'type'    => 'Album',
                    'is_json' => true,
                ]),

                'small_desc' => BaseField::create([
                    'title'   => 'Текст над слайдером',
                    'storage' => 'props.small_desc',
                    'type'    => 'Text',
                ]),

                'nav_title' => BaseField::create([
                    'title'   => 'Заголовок для навигации',
                    'storage' => 'props.nav_title',
                    'type'    => 'String',
                ]),
            ],
        ];
    }

}
