<?php

namespace App\Lib\XFields\Category;

use App\Lib\XFields\Field\BaseField;
use App\Lib\XFields\Category\SeoableCategory;

class GoodCategory extends SeoableCategory
{

    public static function hash() {
        return [
            'products' => 'goods.products',
            'services' => 'goods.services',
        ];
    }


    public static function category_fields()
    {

        return [
            'products' => [
                'price_for_one' => BaseField::create([
                    'title'   => 'Цена',
                    'type'    => 'Integer',
                    'storage' => 'price',
                ]),
            ],
            'services' => [
                'price_for_one' => BaseField::create([
                    'title'   => 'Цена',
                    'type'    => 'Integer',
                    'storage' => 'price',
                ]),
            ],

        ];
    }

    protected static function seo_aliases_to_storages() {
        $old_hash = parent::seo_aliases_to_storages();
        $new_hash = [];

        foreach (config('app.locales') as $locale => $title)
        {
            foreach ($old_hash as $alias => $storage) { $new_hash["{$alias}_{$locale}"] = "{$storage}_{$locale}"; }
        }

        return $new_hash;
    }

}
