<?php

namespace App\Lib\XFields\Category;

use App\Lib\XFields\Field;

class Base
{

    public function __construct($id, $l12n) {
        $this->id = $id;
        $this->l12n = $l12n;
    }

    public function title($tail = 'singular')
    {
        $localize_key = $tail ? implode('.', [$this->l12n, $tail]) : $this->l12n;
        return trans( $localize_key );
    }


    public static function create($id, $titles) {
        return new static($id, $titles);
    }


    public function fields()
    {
        return $this->id ? static::config()[$this->id] : [];
    }


    public function siblings()
    {
        return static::all()->except($this->id);
    }


    public static function hash()
    {
        // список локализованных категорий, определить в наследнике
        return [];
    }


    public static function ids()
    {
        return collect(static::hash())->keys()->all();
    }


    public static function all() {
        // внутри колбека коллекции сбивается namespace, поэтому захватываем нужный namespace заранее
        $class = static::class;

        return collect( static::hash() )->map( function ( $l12n, $id ) use ( $class )  {
            return new $class($id, $l12n);
        });
    }

    public static function find($id) {

        return static::all() -> filter( function ($category) use ( $id ) {
            return $category->id == $id;
        }) -> first();
    }


    public static function config()
    {
        // конфиг уникальных полей для каждой из категорий, определить в наследнике
        return [];
    }


}
