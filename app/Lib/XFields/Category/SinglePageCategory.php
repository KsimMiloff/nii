<?php

namespace App\Lib\XFields\Category;

use App\Lib\XFields\Field\BaseField;
use App\Lib\XFields\Category\SeoableCategory;

class SinglePageCategory extends GoodCategory
{

    public static function hash() {

        return [
            'welcome' => 'single_pages.welcome',
            'contacts' => 'single_pages.contacts',
        ];
    }


    public static function category_fields()
    {
        return [
            'welcome'  => [

                'slider_ids' => BaseField::create([
                    'title'   => 'Слайдер',
                    'storage' => 'props.image_ids',
                    'type'    => 'Album',
                    'is_json' => true,
                ])
//                'url' => BaseField::create([
//                    'title'   => 'Ссылка',
//                    'storage' => 'props.url',
//                    'type'    => 'String',
//                    'is_json' => true,
//                ]),

            ],

            'contacts'  => [

                'map_code' => BaseField::create([
                    'title'   => 'Код карты',
                    'storage' => 'props.map_code',
                    'type'    => 'Text',
                    'is_json' => true,
                ]),

            ],

        ];
    }

}
