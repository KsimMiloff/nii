<?php

namespace App\Lib;

//use Illuminate\Http\Response;

class Responser
{
    public static function data($code = 200, $data = null)
    {
        return response()->json($data, $code);
    }

    public static function error($code = 400, $message = null)
    {
        // check if $message is object and transforms it into an array
        if (is_object($message)) { $message = $message->toArray(); }

        switch ($code) {
            default:
                $code_message = 'error_occured';
                break;
        }

        $data = array(
            'code'      => $code,
            'message'   => $code_message,
            'data'      => $message
        );

        // return an error
//        $response = new Response();
        return response()->json($data, $code);
    }

}