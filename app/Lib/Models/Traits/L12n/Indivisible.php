<?php

namespace App\Lib\Models\Traits\L12n;
use App\Lib\Models\Traits\L12n\Separated;

use App\Lib\Traits\DynamicMethods;
use App\Lib\Models\Traits\DynamicMutators;

use App\Lib\Locale;

trait Indivisible
{
    use Separated;

    // protected static $fields_to_locale = []; // Нужно объявить в главном классе

    public function __construct() {
        foreach(static::$fields_to_locale as $alias)
        {
            $getter = $this->getter_mutator($alias);
            $locale = \App::getLocale();

            // доступ к title_ru|title_en|title_kz, через СВОЙСТВО title
            $this->addMethod($getter, function () use ($alias, $locale) {
                $field = "{$alias}_{$locale}";
                return $this->$field;
            });

            // доступ к title_ru|title_en|title_kz, через МЕТОД title($locale)
            $this->addMethod($alias, function($locale) use ($alias) {
                $field = "{$alias}_{$locale}";
                return $this->$field;
            });

        }
    }

}