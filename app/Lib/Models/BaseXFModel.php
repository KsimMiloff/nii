<?php

namespace App\Lib\Models;

use App\Lib\Traits\DynamicMethods;
use App\Lib\Models\Traits\DynamicMutators;


class BaseXFModel extends BaseModel
{
    use DynamicMethods;
    use DynamicMutators;

    // Laravel Collection, содержащая список сложных (вложенных) полей
    // [storage => [field, attr]]
    private $nested_storages = [];
    // список наших XFields полей
    public $xfields = [];


    public function __construct(array $attributes = []) {
        $this->category_id = array_get($attributes, 'category_id', null);
        $this->add_extended_fields_behavior();
        parent::__construct($attributes);
    }


    public function newFromBuilder($attributes = [], $connection = null)
    {
        // model - это то, что станет this, просто название из родительского класса
        $model = parent::newFromBuilder($attributes, $connection);
        $model->add_extended_fields_behavior();

        return $model;
    }

    // метод добавляющий в model-instance автоматичеcкую работу с XFields
    private function add_extended_fields_behavior () {
        // генерация этих данных - не самая быстрая операция, поэтому кладем результат
        // выполнения в переменные класса, дальше работаетм черех них
        $this->xfields = $this->xfields();
        $this->nested_storages = $this->collect_nested_storages();

        $this->prettify_names_for_validation();

        foreach ( $this->xfields as $alias => $field ) {
            // генерим имена для геттеров/сеттеров в формате, в котором,
            // Eloquent подумает, что это методы-мутаторы для записи/чтения
            $getter = $this->getter_mutator($alias);
            $setter = $this->setter_mutator($alias);

            $this->makeGetter( $getter, $field );
            $this->makeSetter( $setter, $field );
        }
    }


    private function makeGetter($getter, $field)
    {
        $this->addMethod($getter, function () use ($field) {
            $value = $this->getValue($field->storage); // считываем значения из базы
            $value = $field->from_db_format($value); // преобразуем данные из формата бд в формат ExtendedField

            return $value;
        });

    }

    private function makeSetter($setter, $field)
    {
        $this->addMethod( $setter, function ( $value ) use ( $field ) {
            $value = $field->to_db_format( $value ); // преобразуем данные из формата ExtendedField в формат модели
            $this->setValue( $field->storage, $value ); // присваиваем данные в модель
        });
    }

    private function getValue($storage)
    {
        if ( $this->nested_storages->has($storage) )
        {
            list( $field, $attr ) = $this->nested_storages->get( $storage );
            return array_get( $this->$field, $attr, null );
        } else {
            return array_get( $this->attributes, $storage, null );
        }
    }

    private function setValue($storage, $value)
    {
        // если пришли данные для вложенного поля
        if ( $this->nested_storages->has($storage) )
        {
            // получаем имена самого поля в бд и текущего атрибута
            list($field, $attr) = $this->nested_storages->get($storage);

            // фреймворк не дает нам на прямую менять $this->$field поэтому работаем с промежуточной переменной
            $field_attrs = is_array($this->$field) ? $this->$field : [];
            // в полученном списке атрибутов, устанавливаем/заменяем значение текущего атрибута
            // и присваиваем полученный список атрибутов полю
            $this->$field = array_set($field_attrs, $attr, $value);

        } else {
            // все просто, тупо замещаем старое значение новым
            array_set($this->attributes, $storage, $value);
        }

    }

    // не очень быстрая операция, выполняется единожды для каждого экземпляра в методе newFromBuilder
    // для ускорения кладем результат в this->nested_storages
    private function collect_nested_storages() {
        return
            // выдераем storage из настроек xfields
            collect( $this->xfields )->values()->pluck('storage')
            // ищем storage использующие dot notation
            ->filter( function ($storage) {
                return str_contains($storage, '.');
            })
            // превращаем в пару хранилище->родительское хранилище
            ->map( function ($storage) {
                return [
                    $storage => explode('.', $storage)
                ];
            })
            // все :)
            ->unique()->collapse();
    }


    private function prettify_names_for_validation () {
        foreach ( $this->xfields as $alias => $field ) {
            $this->attributeNames[$alias] = $field->title;
        }
    }


    // возвращает массив пар extended_field_name => instance_of (XFields\Field\SomeField)
    public function xfields() {
        return [];
    }

}
