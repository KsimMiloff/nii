<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Lib\Models\Traits\L12n\Indivisible as L12n;

class Seotag extends Model
{

    use L12n {
        L12n::__construct as private __L12nConstruct;
    }

    public $timestamps = false;

    protected $table = 'seotags';

    protected $guarded = [];

    protected static $fields_to_locale = ['title', 'keywords', 'description'];

//    protected $title_prefix = 'Алматинская матрасная фабрика';

    public function __construct($attributes = [])
    {
        $this->__L12nConstruct();
        parent::__construct($attributes);
    }

    public function seoable()
    {
        return $this->morphTo();
    }

    public function getSiteTitleAttribute()
    {
        $title = $this->title ?: $this->seoable->seotitle;

        return $this->title_prefix . ' | ' .$title;
    }
}
