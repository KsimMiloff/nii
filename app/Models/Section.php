<?php

namespace App\Models;

use App\Lib\Models\Traits\LifeCycle;
use App\Lib\Models\Traits\HasXFCategoryMixin;
use App\Lib\Models\Traits\L12n\Separated as L12n;
use App\Lib\XFields\Category\SectionCategory as Category;

use App\Lib\Models\BaseXFModel;
use App\Lib\Nii\Tree;

class Section extends BaseXFModel
{
    const CATEGORY_CLASS = Category::class;

    use LifeCycle {
        LifeCycle::__construct as private __LifeCycleConstruct;
    }

    use HasXFCategoryMixin,
        L12n;

    protected $table = 'sections';

    protected $guarded = [];

    protected $casts = [
        'image_id'   => 'integer',
        'props'      => 'array',
    ];


    protected $attributes = [
        'is_visible' => true,
    ];


    protected $rules = array(
//        'title'       => 'required',
        'locale_id'   => 'required',
        'category_id' => 'required|in:text,image,slider',
        'chain'       => 'required',
    );


    public function scopeByChain($query, $chain) {
        if ( is_array($chain) )
        {
            $chain = implode('/', $chain);
        }
        return $query->where('chain', $chain);
    }


    public function __construct($attributes = [])
    {
        $this->__LifeCycleConstruct();
        parent::__construct($attributes);
    }


    public function xfields()
    {
        return $this->category->fields();
    }


    public function getNodeAttribute()
    {
        $chain = array_get($this->attributes, 'chain', null);
        return Tree::find_by_chain( $chain );
    }


    public function getChainAttibute()
    {
        return $this->node->path(); // удобно хранить массив в mysql мы не можем, поэтому работаем со строкой
    }


}
