<?php

namespace App\Models;

use App\Lib\XFields\Field\BaseField;
use App\Lib\Models\Traits\LifeCycle;
use App\Lib\Models\Traits\SeoableMixin;
use App\Lib\Models\Traits\L12n\Indivisible as L12n;
use App\Lib\Models\Traits\HasXFCategoryMixin;
use App\Lib\XFields\Category\GoodCategory as Category;

use App\Lib\Models\BaseXFModel;


class Good extends BaseXFModel
{

    use LifeCycle {
        LifeCycle::__construct as private __LifeCycleConstruct;
    }

    use SeoableMixin {
        SeoableMixin::__construct as private __SeoableConstruct;
    }

    use L12n {
        L12n::__construct as private __L12nConstruct;
    }

    use HasXFCategoryMixin;

    const SLUG_FROM_FIELD = 'name';

    const CATEGORY_CLASS  = Category::class;

    protected static $fields_to_locale = ['name', 'extra_text', 'desc', 'seo_title', 'seo_keywords', 'seo_description'];

    protected $table = 'goods';

    protected $guarded = [];

    protected $casts = [
        'image_id'    => 'integer',
        'image_ids'   => 'array',
        'props'       => 'array',
        'seted_ids'   => 'array',
    ];


    protected $attributes = [
        'is_visible' => true
    ];


    protected $rules = array(
//        'name' => 'required',
        'category_id' => 'required|in:products,services',
    );



    public function scopeVisible($query) {
        return $query->where('is_visible', true);
    }



    public function __construct($attributes = [])
    {
        $this->__LifeCycleConstruct();
        $this->__L12nConstruct();
        $this->__SeoableConstruct();
        parent::__construct($attributes);
    }


    public function xfields() {
        $xfields = $this->category->fields();

        return $xfields + [
            'image_id' => BaseField::create([
                'title'   => 'Изображение',
                'storage' => 'image_id',
                'type'    => 'Image'
            ]),
//            'slider_ids' => BaseField::create([
//                'title'   => 'Слайдер',
//                'storage' => 'image_ids',
//                'type'    => 'Album',
//                'is_json' => true,
//            ]),
        ];
    }


    public function getPriceAttribute()
    {
        if ($this->category_id == 'mattress') {
            list($w, $h) = explode('x', Category::$mattress_sizes['default']);
            return $this->price_per_square_meter * $w * $h * 0.0001;
        }

        return array_get($this->attributes, 'price', null);
    }


    public function getSetedItemsAttribute() // seted_items
    {
        return static::byState('live')->whereIn('id', $this->seted_ids)->get();
    }


    public function in_my_set($id)
    {
        if (is_array($this->seted_ids)) {
            return in_array($id, $this->seted_ids);
        }
        return false;
    }



    public function slugable() {
        return $this->name; // поле из которого строиться слаг
    }


}
