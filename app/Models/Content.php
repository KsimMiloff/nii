<?php

namespace App\Models;

use App\Lib\Models\Traits\LifeCycle;
use App\Lib\Models\Traits\HasXFCategoryMixin;
use App\Lib\Models\Traits\SeoableMixin;
use App\Lib\Models\Traits\L12n\Separated as L12n;
use App\Lib\XFields\Category\ContentCategory as Category;

use App\Lib\Models\BaseXFModel;


class Content extends BaseXFModel
{
    const SLUG_FROM_FIELD = 'title';
    const CATEGORY_CLASS  = Category::class;

    use LifeCycle {
        LifeCycle::__construct as private __LifeCycleConstruct;
    }

    use SeoableMixin {
        SeoableMixin::__construct as private __SeoableConstruct;
    }

    use HasXFCategoryMixin,
        L12n;

    protected $table = 'contents';

    protected $guarded = [];

    protected $casts = [
        'image_id'   => 'integer',
        'props'      => 'array',
    ];


    protected $attributes = [
        'is_visible' => true,
    ];


    protected $rules = array(
        'title'       => 'required',
        'locale_id'   => 'required',
        'category_id' => 'required|in:news',
//        'desc'        => 'required',
    );



    public function scopeVisible($query) {
        return $query->where('is_visible', true);
    }


    public function scopeForMainPage($query) {
        return $query->where('is_visible', true);
    }


    public function __construct($attributes = [])
    {
        $this->__LifeCycleConstruct();
        $this->__SeoableConstruct();
        parent::__construct($attributes);
    }


    public function xfields()
    {
        return $this->category->fields();
    }

}
