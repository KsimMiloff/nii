<?php

namespace App\Models;

use App\Lib\Models\Traits\HasXFCategoryMixin;
use App\Lib\Models\Traits\SeoableMixin;
use App\Lib\Models\Traits\L12n\Indivisible as L12n;
use App\Lib\XFields\Category\SinglePageCategory as Category;

use App\Lib\Models\BaseXFModel;


class SinglePage extends BaseXFModel
{
    const SLUG_FROM_FIELD = 'title';
    const CATEGORY_CLASS  = Category::class;


    use SeoableMixin {
        SeoableMixin::__construct as private __SeoableConstruct;
    }

    use L12n {
        L12n::__construct as private __L12nConstruct;
    }

    use HasXFCategoryMixin;

    protected $table = 'single_pages';

    protected $guarded = [];

    protected static $fields_to_locale = ['title', 'desc', 'seo_title', 'seo_keywords', 'seo_description'];

    protected $casts = [
        'image_id'   => 'integer',
        'props'      => 'array',
    ];


    protected $attributes = [
        'is_visible' => true,
    ];


    protected $rules = array(
        'category_id' => 'required|in:welcome,contacts',
//        'desc'        => 'required',
    );


    public function __construct($attributes = [])
    {
//        print_r($attributes);exit;

        $this->__L12nConstruct();
        $this->__SeoableConstruct();
        parent::__construct($attributes);
    }


    public function scopeVisible($query) {
        return $query->where('is_visible', true);
    }


    public function scopeForMainPage($query) {
        return $query->where('is_visible', true);
    }


    public function xfields()
    {
        return $this->category->fields();
    }

}
