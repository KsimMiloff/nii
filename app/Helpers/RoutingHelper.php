<?php

function locale_route($name, $parameters = [], $absolute = false) {
    $locale = isset( $parameters['locale'] ) ? $parameters['locale'] : App::getLocale();
    return route($name, array_merge($parameters, [
        'locale' => $locale
    ]), $absolute);
}