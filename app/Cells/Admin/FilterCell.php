<?php


class AdminFilterCell
{
    public static function show($filter_list)
    {
        $filters = [];
        foreach( $filter_list as $template => $params )
        {
            if (is_int( $template )) {
                $template = $params;
                $params = [];
            }
            $filters[] = view("admin.cells.filter.{$template}", $params);
        }

        return view('admin.cells.filter.show', [
            'filters' => implode('', $filters)
        ]);
    }
}

