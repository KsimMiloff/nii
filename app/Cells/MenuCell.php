<?php

use App\Lib\Models\Traits\Singleton;


class MenuCell
{
    use Singleton;

    const STRUCT_PATH  = 'static_data/menu_struct.yaml';

    private function __construct()
    {
        $filename     = database_path(static::STRUCT_PATH);
        $file_content = File::get($filename);

        $this->struct  = Parser::yaml($file_content);
    }

    public static function show() {

        $menu_items = [];
        foreach (static::getInstance()->struct as $item)
        {
            $menu_items[]=  static::menu_item($item);
        }

        return view('cells.menu.show', [
            'menu_items' => $menu_items
        ]);
    }

    public static function menu_item($item)
    {

        $result = [
            'url' => static::item_url($item),
            'title' => static::item_title($item),
        ];


        $url_chunks = explode( '/', $result['url'] );
        $url_chunks = array_filter( $url_chunks );

        $chunck_bucket = [];
        $active = false;


        foreach($url_chunks as $chunck) {
            $chunck_bucket[]= $chunck;
            $glued_path = implode('/', $chunck_bucket);


            if ( count( $chunck_bucket ) > 1 && Request::is("$glued_path*") ) // Request::is("$glued_path/*")
            {
                $active = true;
                break;
            }

        }

        $result['active'] = $active;

        return $result;
    }

    private static function item_url($item)
    {


        $url = isset( $item['url']['route'] ) ? locale_route( $item['url']['route'], $item['url']['params'] ) :
            (isset( $item['url']['link'] ) ? $item['url']['link'] : null);

        return $url;
    }


    private static function item_title($item)
    {
        $title = isset( $item['title'] ) ? trans( $item['title'] ) : null;
        if ( isset( $item['title_method'] ) ) {
            $method = $item['title_method'];
            $title  = static::$method();
        }

        return $title;
    }

}

