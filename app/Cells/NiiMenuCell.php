<?php

use App\Lib\Nii\Tree;

class NiiMenuCell
{

    public static function show($current_node)
    {
        $first_parent_node = static::tree()->find_by_chain( $current_node->chain[0] );
        return static::nav_subs($first_parent_node, $current_node);
    }

    public static function nav_subs($top_node, $current_node)
    {
        $class = [$top_node->level() == 1 ? 'green' : 'white'];

        return view('cells.nii_menu.show', [
            'current_node' => $current_node,
            'top_node' => $top_node,
            'class' => implode(' ', $class),

        ]);
    }

    private static function tree() {
        return Tree::getInstance();
    }
}

