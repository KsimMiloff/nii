<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Lib\Nii\Tree as NiiTree;

//Route::group(['middleware' => ['web']], function () {

    Route::get('/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::post('/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@postLogin']);
    Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

    Route::group(['namespace' => 'File'], function()
    {
        Route::post('files/wisiwyg_upload}',
            ['as' => 'files.wisiwyg_upload', 'uses' => 'FilesController@wisiwyg_upload']
        );

        Route::get('images/{id}/resized/{size}/{crop?}',
            ['as' => 'images.resized', 'uses' => 'ImagesController@resize']
        );

        Route::resource('images', 'ImagesController', ['except' => ['destroy']]);
        Route::resource('files', 'FilesController', ['except' => ['destroy']]);
    });


    Route::get('admin', function () {
        return redirect('/admin/nii');
    });


    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function()
    {

        Route::get('nii/{locale?}/{chain?}',
            ['as' => 'admin.nii.index', 'uses' => 'NiiController@index']
        )->where([
            'locale' => 'ru|en|kz',
            'chain'  => NiiTree::path_collection()->implode('|')
        ]);


        Route::get('sections/create/{locale}/{category_id}/{chain}',
            ['as' => 'admin.sections.create', 'uses' => 'SectionsController@create']
        )->where([
            'locale' => 'ru|en|kz',
            'chain'  => NiiTree::path_collection()->implode('|')
        ]);

        Route::get('goods/create/{category_id?}', ['as' => 'admin.goods.create', 'uses' => 'GoodsController@create']);
        Route::resource('goods', 'GoodsController', ['except' => ['create']]);

        Route::get('contents/create/{category_id?}', ['as' => 'admin.contents.create', 'uses' => 'ContentsController@create']);
        Route::resource('contents', 'ContentsController', ['except' => ['create']]);

        Route::post('sections/orderize', ['as' => 'admin.sections.orderize', 'uses' => 'SectionsController@orderize']);
        Route::resource('sections', 'SectionsController', ['except' => ['create']]);

        Route::get('singe_pages/{category_id}', [
            'as' => 'admin.single_pages',
            'uses' => 'SinglePagesController@edit'
        ])->where([
            'category_id'  => 'welcome|contacts'
        ]);

        Route::resource('single_pages', 'SinglePagesController', ['only' => ['store', 'update']]);

        Route::resource('users', 'UsersController', ['except' => ['show', 'destroy']]);

    });


    Route::group(['prefix' => '{locale?}','middleware' => ['locale']], function () {


        Route::get('/', 'WelcomeController@index');


        Route::get('{chain}',
            ['as' => 'nii.index', 'uses' => 'NiiController@index']
        )->where([
            'locale' => 'ru|en|kz',
            'chain' => NiiTree::path_collection()->implode('|')
        ]);


        Route::group(['prefix' => 'products-n-services','middleware' => ['locale']], function () {
            Route::get('{category_id}',
                ['as' => 'goods.index', 'uses' => 'GoodsController@index']
            )->where([
                'category_id' => 'products|services'
            ]);
        });




        Route::get('{single_pages_category}',
            ['as' => 'single_pages.show', 'uses' => 'SinglePagesController@show']
        )->where([
            'single_pages_category' => 'welcome|contacts'
        ]);


        Route::get('{content_category}',
            ['as' => 'contents.index', 'uses' => 'ContentsController@index']
        )->where([
            'content_category' => 'news|news'
        ]);

        Route::get('{content_category}/{id}',
            ['as' => 'contents.show', 'uses' => 'ContentsController@show']
        )->where([
            'content_category' => 'news'
        ]);


    });


//});
