<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Lib\Nii\Tree as NiiTree;
use App\Models\Section;
use App\Lib\XFields\Category\SectionCategory;

use App\Http\Requests;

class NiiController extends Controller
{
    public function index(Request $request, $current_locale='ru', $chain=[])
    {
        $root_node = NiiTree::root();

        $current_nodes = $chain ? NiiTree::find_each_by_chain($chain) : [$root_node->branches[0]];
        $current_node  = array_last( $current_nodes );

        array_unshift( $current_nodes, $root_node );

        $sections = Section::live()
            ->byChain($current_node->chain)
            ->where('locale_id', $current_locale)
            ->orderBy('position', 'desc')
            ->get();


        return view('admin.nii.index', [
            'root_node' => $root_node,
            'current_node' => $current_node,
            'current_nodes' => $current_nodes,
            'sections' => $sections,
            'categories'  => SectionCategory::all(),
            'current_locale' => $current_locale
        ]);
    }

}
