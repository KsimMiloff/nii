<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Good;
use App\Lib\XFields\Category\GoodCategory;

use App\Http\Controllers\Admin\Traits\SpecialActions;

class GoodsController extends Controller
{
    use SpecialActions;

    const VIEWS_FOLDER = 'goods';
    const VIEW_VAR_NAME = 'good';

    public function index(Request $request)
    {
        $state       = $request->get('state', 'live');
        $category_id = $request->session()->get('good.category_id');


        $goods      = Good::byState($state);
        if ($category_id) {
            $goods      = $goods->byCategoryId($category_id);
        }

        return view('admin.goods.index', [
            'goods'       => $goods->orderBy('created_at')->get(),
            'categories'  => GoodCategory::all(),
            'category_id' => $category_id,
            'state'       => $state
        ]);
    }

    public function create(Request $request, $category_id)
    {
        $good = new Good(['category_id' => $category_id]);
        return view('admin.goods.form', ['good' => $good]);
    }


    public function store(Request $request)
    {
        $good = new Good($this->good_params());

        return $this->run_action( $good );

    }

    public function edit(Request $request, $id)
    {
        $good = Good::find($id);
        return view('admin.goods.form', ['good'   => $good,]);
    }


    public function update(Request $request, $id)
    {
        $good = Good::find($id)
            ->fill($this->good_params());

        return $this->run_action( $good );
    }


    private function good_params()
    {
        $params  = request()->good ?: [];
        return $params;
    }

}
