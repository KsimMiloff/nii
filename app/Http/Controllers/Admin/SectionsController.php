<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Section;
use App\Lib\XFields\Category\SectionCategory;

use App\Lib\Responser;

class SectionsController extends Controller
{

    public function create(Request $request, $locale, $category_id, $chain)
    {
        $section = new Section([
            'locale_id' => $locale,
            'category_id' => $category_id,
            'chain' => $chain
        ]);

        return view('admin.sections.form', [
            'section' => $section,
        ]);
    }


    public function store(Request $request)
    {

        $section = new Section($this->section_params());


        if ( $section->save() ) {
            return view('admin.sections._list_item', [
                'section' => $section
            ]);

        } else {
            return Responser::error(400, $section->errors());
        }

    }


    public function edit(Request $request, $id)
    {

        $section = Section::find($id);

        return view('admin.sections.form', [
            'section' => $section,
        ]);
    }


    public function update(Request $request, $id)
    {
        $section = Section::find($id)
            ->fill($this->section_params());

        if ( $section->save() ) {
            return view('admin.sections._list_item', [
                'section' => $section
            ]);

        } else {
            return Responser::error(400, $section->errors());
        }
    }


    public function destroy(Request $request, $id)
    {
        $section = Section::find($id);

        if ( $section ) {
            $section->move_to_archive();
            return Responser::data(200, ['id' => $section->id]);
        } else
        {
            return Responser::error(404, $section->errors());
        }
    }


    public function orderize(Request $request)
    {
        // читаем массив в обратном порядке, чтобы в коллекции Model::orderBy('position', 'desc') все объекты
        // у которых position == null были вконце
        $ids_in_new_order  = array_reverse( $request->order );
        $sections_to_order = Section::whereIn('id', $ids_in_new_order)->get(); // объекты для смыны порядка

//        $not_existed_ids   = array_diff( $ids_in_new_order , $sections_to_order->pluck('id')->toArray() ); // id не найденных объектов
//        $ids_in_new_order  = array_diff( $ids_in_new_order, $not_existed_ids ); // исключаем не найденные id
        $ids_in_new_order  = array_flip( $ids_in_new_order ); // меням местами ключ=значение для упрощения поиска

        $sections_to_order->each ( function( $section ) use ( $ids_in_new_order ) {
            $section->position = $ids_in_new_order[$section->id];
            if (! $section->save(['validate' => false]))
            {
                return Responser::error(404, []);
            }

        });

        return Responser::data(200);
    }

    private function section_params()
    {
        return request()->section;
    }


}
