<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\SinglePage;
use App\Models\Content;
use App\Lib\Nii\Tree as NiiTree;


class WelcomeController extends Controller
{
    public function index()
    {
        $page = SinglePage::where( 'category_id', 'welcome' )->first();

        $news = Content::live()->visible()->localed()
            ->byCategoryId('news')
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();

        $node = NiiTree::find_by_chain('research');

        return view('welcome', [
            'page' => $page,
            'news' => $news,
            'node' => $node
        ]);
    }



}
