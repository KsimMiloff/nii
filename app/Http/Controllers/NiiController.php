<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Lib\Nii\Tree as NiiTree;
use App\Models\Section;

use App\Http\Requests;

class NiiController extends Controller
{
    public function index(Request $request, $locale, $chain)
    {
        $node = NiiTree::find_by_chain($chain);

        $sections = Section::live()
            ->localed()
            ->byChain($node->chain)
            ->orderBy('position', 'desc')
            ->get();

        return view('nii.index', [
            'node' => $node,
            'sections' => $sections,
        ]);
    }



}
