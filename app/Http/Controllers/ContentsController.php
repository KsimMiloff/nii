<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Content;
use App\Lib\XFields\Category\ContentCategory;
use Mockery\CountValidator\Exception;


class ContentsController extends Controller
{

    public function index(Request $request, $locale, $category_id)
    {
        try {
            $years = [
                'start' => Content::live()->visible()->oldest()->localed()->first()->created_at->year,
                'end'   => Content::live()->visible()->latest()->localed()->first()->created_at->year
            ];
        } catch( \Exception $e ){
            $years = ['start' => date('Y'), 'end' => date('Y') ];
        }

        $current_year = $request->year ? $request->year : $years['end'];

        $category = ContentCategory::find($category_id);
        $contents = Content::live()->visible()->localed()
            ->byCategoryId($category_id)
            ->whereYear('created_at', '=', $current_year)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('contents.index', [
            'contents' => $contents,
            'category' => $category,
            'years' => $years,
            'current_year' => $current_year
        ]);
    }

    public function show(Request $request, $locale, $category_id, $id)
    {
        $content = Content::live()->visible()->localed()
            ->bySlug($id)->first();

        return view('contents.show', [
            'content' => $content,
        ]);
    }

}
