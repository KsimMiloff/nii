<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Good;

class GoodsController extends Controller
{

    public function index(Request $request, $locale, $category_id)
    {

        $goods = Good::live()->visible()
            ->byCategoryId($category_id)
            ->orderBy('created_at')
            ->get();

        return view('goods.index', [
            'goods'       => $goods,
            'category_id' => $category_id,
        ]);
    }

}
