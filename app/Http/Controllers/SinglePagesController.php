<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\SinglePage;
use App\Lib\XFields\Category\SinglePageCategory;


class SinglePagesController extends Controller
{


    public function show(Request $request, $locale, $category_id)
    {
        $page = SinglePage::where( 'category_id', $category_id )->first();

        return view("single_pages.show.$category_id", [
            'page' => $page,
        ]);
    }

}
