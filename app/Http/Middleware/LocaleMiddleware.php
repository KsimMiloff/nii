<?php

namespace App\Http\Middleware;

use Closure;
use App;
//use Session;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->locale)
        {
            $request->session()->put('locale', $request->locale);
        }

        $locale = $request->session()->get('locale') ?: App::getLocale();

        if (! locale_is_valid($locale)) {
            abort(404);
        }

        App::setLocale($locale);

//        $request->merge(['locale' => App::getLocale()]);

        return $next($request);
    }
}


function locale_is_valid($locale) {
    return array_key_exists($locale, config('app.locales'));
}