<?php

namespace App\Http\Middleware;

use Closure;
use App\Seotag;

class SeotagsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $seotags = new StdClass([
            'title' => trans('seotags.default.title'),
            'keywords' => trans('seotags.default.keywords'),
            'descriptions' => trans('seotags.default.descriptions')
        ]);
        view()->share('seotags', $seotags);
        return $next($request);
    }
}
