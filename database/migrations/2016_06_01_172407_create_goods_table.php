<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_ru');
            $table->string('name_en');
            $table->string('name_kz');

            $table->string('slug')->nullable();
            $table->float('price')->nullable();
            $table->string('category_id');
            $table->string('state');

            $table->text('extra_text_ru');
            $table->text('extra_text_en');
            $table->text('extra_text_kz');

            $table->text('desc_ru');
            $table->text('desc_en');
            $table->text('desc_kz');

            $table->string('image_id')->nullable();
            $table->text('image_ids')->nullable();
            $table->text('props')->nullable();
            $table->boolean('is_visible')->nullable();

            $table->text('seo_meta');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('goods');
    }
}
