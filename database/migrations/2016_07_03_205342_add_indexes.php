<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexes extends Migration
{
    public $indexes = [
        'files'        => ['id'],
        'contents'     => ['id', 'slug', 'category_id', 'state', 'is_visible', 'locale_id'],
        'sections'     => ['id', 'slug', 'category_id', 'chain', 'state', 'is_visible'],
        'goods'        => ['id', 'slug', 'category_id', 'state', 'is_visible'],
        'single_pages' => ['id', 'slug', 'category_id', 'state', 'is_visible'],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('files', function (Blueprint $table) {
            foreach ( $this->indexes['files'] as $index) $table->index($index);
        });

        Schema::table('contents', function (Blueprint $table) {
            foreach ( $this->indexes['contents'] as $index) $table->index($index);
        });

        Schema::table('sections', function (Blueprint $table) {
            foreach ( $this->indexes['sections'] as $index) $table->index($index);
        });

        Schema::table('goods', function (Blueprint $table) {
            foreach ( $this->indexes['goods'] as $index) $table->index($index);
        });

        Schema::table('single_pages', function (Blueprint $table) {
            foreach ( $this->indexes['single_pages'] as $index) $table->index($index);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files', function (Blueprint $table) {
            foreach ( $this->indexes['files'] as $index) $table->dropIndex("files_".$index."_index");
        });

        Schema::table('contents', function (Blueprint $table) {
            foreach ( $this->indexes['contents'] as $index) $table->dropIndex("contents_".$index."_index");
        });

        Schema::table('sections', function (Blueprint $table) {
            foreach ( $this->indexes['sections'] as $index) $table->dropIndex("sections_".$index."_index");
        });

        Schema::table('goods', function (Blueprint $table) {
            foreach ( $this->indexes['goods'] as $index) $table->dropIndex("goods_".$index."_index");
        });

        Schema::table('single_pages', function (Blueprint $table) {
            foreach ( $this->indexes['single_pages'] as $index) $table->dropIndex("single_pages_".$index."_index");
        });
    }
}
