<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWelcome extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('single_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();

            $table->string('title_ru');
            $table->string('title_en');
            $table->string('title_kz');


            $table->text('desc_ru');
            $table->text('desc_en');
            $table->text('desc_kz');

            $table->string('category_id');
            $table->string('state');
            $table->text('props')->nullable();
            $table->boolean('is_visible')->nullable();

            $table->text('seo_meta');

            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('single_pages');
    }
}
