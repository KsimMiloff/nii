$(function() {
    $nav = $("#nav-col .nav");
    fixed_class = "nav-scrolled";
    eloffset = $nav.offset().top;


    $(window).scroll(function () {
        if ($(this).scrollTop() > eloffset - 40) {
            $nav.addClass(fixed_class);
        } else {
            $nav.removeClass(fixed_class);
        }
    });

});